from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# view to display all TodoLists
def todo_list_list(request):
    # create a variable that stores all instances of the TodoList Model
    todo_lists = TodoList.objects.all()

    # then store this variable as the value in our context dict
    context = {
        "todo_lists": todo_lists,
    }

    # then return the correct render
    return render(request, "todos/todo_list_list.html", context)


# view to display the items in a specific todo list
def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)

    context = {
        "todo_list": todo_list,
    }

    # then return the correct render
    return render(request, "todos/todo_list_detail.html", context)


# view to create a new todo list
def todo_list_create(request):
    # if the request is a POST...
    if request.method == "POST":
        # then we fill in a form variable with the details of the request
        form = TodoListForm(request.POST)
        # we then check if the form submission was valid
        if form.is_valid():
            # save the form to a variable to redirect to a newly created page
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    # otherwise the request is a GET...
    else:
        # so we show the form
        form = TodoListForm()

    # we then put the form in the context...
    context = {
        "form": form,
    }

    # and return the create template
    return render(request, "todos/todo_list_create.html", context)


# view to update an existing todo list
def todo_list_update(request, id):
    # first we need to pull the todolist that needs to be edited
    todo_list_instance = TodoList.objects.get(id=id)
    # if the request is a POST...
    if request.method == "POST":
        # then create a form variable associated with this specific todo list
        form = TodoListForm(request.POST, instance=todo_list_instance)
        # if the form is valid...
        if form.is_valid():
            # then we save since there is no additional detail to add
            form.save()
            return redirect("todo_list_detail", id=id)

    # otherwise the request is a GET...
    else:
        # so we show the form, specific to this todo list
        form = TodoListForm(instance=todo_list_instance)

    # we then put the form in the context...
    context = {
        "form": form,
    }

    # and return the correct render
    return render(request, "todos/todo_list_update.html", context)


# view to delete an existing todo list
def todo_list_delete(request, id):
    # first we need to pull the todolist that is to be deleted
    todo_list_instance = TodoList.objects.get(id=id)
    # then if the request is a POST...
    if request.method == "POST":
        # we delete the instance
        todo_list_instance.delete()
        # and then we redirect to the list page
        return redirect("todo_list_list")

    # otherwise it's a GET request...
    else:
        # and we return the correct render
        return render(request, "todos/todo_list_delete.html")


# view to create a todo item
def todo_item_create(request):
    # if the request is a POST...
    if request.method == "POST":
        # create form to ingest the info in the form submission
        form = TodoItemForm(request.POST)
        # if the form submisison is valid...
        if form.is_valid():
            # then save the form
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)

    # otherwise it's a GET...
    else:
        # so we create an empty form
        form = TodoItemForm()

    # create the context
    context = {
        "form": form,
    }

    # and return the correct render
    return render(request, "todos/todo_item_create.html", context)


# view to update a todo item
def todo_item_update(request, id):
    # first get the item instance that we will be updating
    todo_item_instance = TodoItem.objects.get(id=id)
    # now we check if the request is a POST...
    if request.method == "POST":
        # create form to input the updated info on this todo item
        form = TodoItemForm(request.POST, instance=todo_item_instance)
        # we then save the form to a new variable for our redirect
        todo_item = form.save()
        # and we redirect to the todo detail page
        return redirect("todo_list_detail", id=todo_item.list.id)

    # otherwise it's a GET request...
    else:
        # so we create a form with the todo item's info
        form = TodoItemForm(instance=todo_item_instance)

    # and put that form into context
    context = {
        "form": form,
    }

    # and return the correct render
    return render(request, "todos/todo_item_update.html", context)
