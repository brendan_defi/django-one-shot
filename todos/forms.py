from django.forms import ModelForm
from todos.models import TodoList, TodoItem


# a form to create a new todo list
class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


# a form to create a new todo item
class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
