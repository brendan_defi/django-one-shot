from django.contrib import admin
from todos.models import TodoList, TodoItem


# register the TodoList Model
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
    ]


# register the TodoItem Model
@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]
